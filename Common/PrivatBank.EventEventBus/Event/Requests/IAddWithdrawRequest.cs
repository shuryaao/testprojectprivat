﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivatBank.EventEventBus.Event.Requests
{
    interface IAddWithdrawRequest
    {
        public Guid Id { get;  }

        public DateTimeOffset TimeStamp { get; }

        public string ClientId { get; }

        public string DepartemntAddress { get; }

        public double Amount { get; }

        public string Currency { get; }
    }
}
