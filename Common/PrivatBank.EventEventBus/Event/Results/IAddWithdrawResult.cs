﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivatBank.EventEventBus.Event.Results
{
    interface IAddWithdrawResult
    {
        public Guid Id { get; }

        public DateTimeOffset TimeStamp { get; }

        public string WithdrawId { get; }
    }
}
