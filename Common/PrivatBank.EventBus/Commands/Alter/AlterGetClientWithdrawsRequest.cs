﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivatBank.EventBus.Commands.Alter
{
    public class AlterGetClientWithdrawsRequest : EventBase
    {
        public AlterGetClientWithdrawsRequest()
        {
            TimeStamp = DateTime.UtcNow;
        }
        public string ClientId { get; set; }

        public string DepartemntAddress { get; set; }
    }
}
