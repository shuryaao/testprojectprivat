﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivatBank.EventBus.Commands.Alter
{
    public class AlterAddWithdrawResponse : EventBase
    {
        public AlterAddWithdrawResponse()
        {
            TimeStamp = DateTime.UtcNow;
        }

        public string WithdrawId{ get; set; }
    }
}
