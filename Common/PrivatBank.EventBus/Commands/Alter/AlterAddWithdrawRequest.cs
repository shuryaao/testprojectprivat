﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivatBank.EventBus.Commands.Alter
{
    public class AlterAddWithdrawRequest : EventBase
    {
        public AlterAddWithdrawRequest()
        {
            TimeStamp = DateTime.UtcNow;
        }

        public string ClientId { get; set; }

        public string DepartemntAddress { get; set; }

        public double Amount  { get; set; }

        public string Currency { get; set; }
    }
}
