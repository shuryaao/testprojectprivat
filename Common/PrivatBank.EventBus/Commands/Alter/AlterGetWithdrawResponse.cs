﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivatBank.EventBus.Commands.Alter
{
    public class AlterGetWithdrawsResponse : EventBase
    {
        public AlterGetWithdrawsResponse()
        {
            TimeStamp = DateTime.UtcNow;
        }

        public IEnumerable<Withdraw> Withdraws { get; set; }
    }

    public class Withdraw{
        public string WithdrawId { get; set; }

        public double Amount { get; set; }

        public string Currency { get; set; }

        public string Status { get; set; }
    }
}
