﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivatBank.EventBus
{
    public abstract class EventBase
    {
        public string Id { get; set; }
        public DateTimeOffset TimeStamp { get; protected set; }
    }
}
