﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Models
{
    public class AddWithdrawRequest
    {
        [Required]
        [JsonPropertyName("client_id")]
        public string ClientId { get; set; }

        [Required]
        [JsonPropertyName("departemnt_address")]
        public string DepartemntAddress { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Amount must be bigger than {1}")]
        public double Amount { get; set; }

        [Required]
        public string Currency { get; set; }
    }
}
