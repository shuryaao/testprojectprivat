﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Models
{
    public class GetWithdrawByClientRequest
    {
        [Required]
        [JsonPropertyName("client_id")]
        public string ClientId { get; set; }

        [Required]
        [JsonPropertyName("departemnt_address")]
        public string DepartemntAddress { get; set; }
    }
}
