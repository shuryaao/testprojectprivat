﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Models
{
    public class GetWithdrawByIdRequest
    {
        [Required]
        [JsonPropertyName("request_id")]
        public string WithdrawId { get; set; }
    }
}
