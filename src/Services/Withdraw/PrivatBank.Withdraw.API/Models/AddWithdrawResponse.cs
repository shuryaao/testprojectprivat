﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Models
{
    public class AddWithdrawResponse
    {
        public string RequestId{ get; set; }
    }
}
