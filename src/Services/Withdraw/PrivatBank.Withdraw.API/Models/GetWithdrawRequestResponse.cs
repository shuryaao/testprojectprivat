﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Models
{
    public class GetWithdrawRequestResponse
    {
        public string WithdrawId { get; set; }

        public double Amount { get; set; }

        public string Currency { get; set; }

        public string Status { get; set; }
    }
}
