﻿using PrivatBank.Withdraw.API.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Services
{
    public class CallbackMapperService : ICallbackMapperService
    {
        private readonly ConcurrentDictionary<string, TaskCompletionSource<object>>
            callbackAddWithdrawMapper = new ConcurrentDictionary<string, TaskCompletionSource<object>>();

        public bool AddCallback(string correlationId, TaskCompletionSource<object> task)
        {
            return callbackAddWithdrawMapper.TryAdd(correlationId.ToString(), task);
        }

        public bool IsSuckTaskExist(string correlationId)
        {
           return callbackAddWithdrawMapper.ContainsKey(correlationId);
        }

        public bool SetValue(string correlationId, object obj)
        {
            var suchTaskExists = callbackAddWithdrawMapper.TryRemove(correlationId,
                                                                    out var tcs);

            if (!suchTaskExists) return false;

            return tcs.TrySetResult(obj);
        }
    }
}
