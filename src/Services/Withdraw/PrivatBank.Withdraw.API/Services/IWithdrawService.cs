﻿using MassTransit;
using PrivatBank.EventBus.Commands.Alter;
using PrivatBank.Withdraw.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Services
{
    public interface IWithdrawService : IConsumer<AlterAddWithdrawResponse>, IConsumer<AlterGetWithdrawsResponse>
    {
        Task<AddWithdrawResponse> AddWithdraw(AddWithdrawRequest withdrawRequest);
        Task<IEnumerable<GetWithdrawRequestResponse>> AddWithdraw(GetWithdrawByIdRequest withdrawRequest);
        Task<IEnumerable<GetWithdrawRequestResponse>> AddWithdraw(GetWithdrawByClientRequest withdrawRequest);
        Task Consume(ConsumeContext<AlterAddWithdrawResponse> context);
    }
}
