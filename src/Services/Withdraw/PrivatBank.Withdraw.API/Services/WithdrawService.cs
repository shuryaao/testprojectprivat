﻿using MassTransit;
using Microsoft.Extensions.Options;
using PrivatBank.EventBus.Commands.Alter;
using PrivatBank.Withdraw.API.Config;
using PrivatBank.Withdraw.API.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Services
{
    public class WithdrawService : IWithdrawService
    {
        private readonly IBus eventBus;
        private readonly EventBusConfig eventBusConfig;
        private readonly ICallbackMapperService callbackMapper;

        public WithdrawService(IBus eventBus, IOptions<EventBusConfig> eventBusConfig, ICallbackMapperService callbackMapper)
        {
            this.eventBus = eventBus;
            this.eventBusConfig = eventBusConfig.Value;
            this.callbackMapper = callbackMapper;
        }

        public async Task<AddWithdrawResponse> AddWithdraw(AddWithdrawRequest withdrawRequest)
        {
            return  await CreateAddWithdrawAlterMessage(withdrawRequest.ClientId, withdrawRequest.DepartemntAddress, withdrawRequest.Amount, withdrawRequest.Currency);
        }

        public async Task<IEnumerable<GetWithdrawRequestResponse>> AddWithdraw(GetWithdrawByIdRequest withdrawRequest)
        {
            return await CreateGetWithdrawByIdAlterMessage(withdrawRequest.WithdrawId);
        }

        public async Task<IEnumerable<GetWithdrawRequestResponse>> AddWithdraw(GetWithdrawByClientRequest withdrawRequest)
        {
            return await CreateGetClientWithdrawsAlterMessage(withdrawRequest.ClientId, withdrawRequest.DepartemntAddress);
        }

        private async Task<AddWithdrawResponse> CreateAddWithdrawAlterMessage(string clientId, string departemtAddress, double amount, string currency)
        {
            var publishEndpoint = await eventBus.GetSendEndpoint(new Uri($"queue:{eventBusConfig.AlterWithdrawRequestQueue}"));

            var corrId = Guid.NewGuid().ToString();
            var tcs = new TaskCompletionSource<object>();
            callbackMapper.AddCallback(corrId, tcs);

            await publishEndpoint.Send(new AlterAddWithdrawRequest
            {
                Id = corrId,
                Amount = amount,
                ClientId = clientId,
                Currency = currency,
                DepartemntAddress = departemtAddress
            });

            return (AddWithdrawResponse)await tcs.Task;
        }

        private async Task<IEnumerable<GetWithdrawRequestResponse>> CreateGetWithdrawByIdAlterMessage(string withdrawId)
        {
            var publishEndpoint = await eventBus.GetSendEndpoint(new Uri($"queue:{eventBusConfig.AlterGetByIdWithdrawRequestQueue}"));

            var corrId = Guid.NewGuid().ToString();
            var tcs = new TaskCompletionSource<object>();
            callbackMapper.AddCallback(corrId, tcs);

            await publishEndpoint.Send(new AlterGetByIdWithdrawRequest
            {
                Id = corrId,
                WithdrawId = withdrawId
            });

            return (IEnumerable<GetWithdrawRequestResponse>)await tcs.Task;
        }

        private async Task<IEnumerable<GetWithdrawRequestResponse>> CreateGetClientWithdrawsAlterMessage(string clientId, string departemntAddress)
        {
            var publishEndpoint = await eventBus.GetSendEndpoint(new Uri($"queue:{eventBusConfig.AlterGetClientWithdrawRequestQueue}"));

            var corrId = Guid.NewGuid().ToString();
            var tcs = new TaskCompletionSource<object>();
            callbackMapper.AddCallback(corrId, tcs);

            await publishEndpoint.Send(new AlterGetClientWithdrawsRequest
            {
                Id = corrId,
                ClientId = clientId,
                DepartemntAddress = departemntAddress
            });

            return (IEnumerable<GetWithdrawRequestResponse>)await tcs.Task;
        }

        public async Task Consume(ConsumeContext<AlterGetWithdrawsResponse> context)
        {
            if (callbackMapper.IsSuckTaskExist(context.Message.Id))
                callbackMapper.SetValue(context.Message.Id, context.Message.Withdraws
                    .Select(withdraw => new GetWithdrawRequestResponse
                    {
                        WithdrawId = withdraw.WithdrawId,
                        Amount = withdraw.Amount,
                        Status = withdraw.Status,
                        Currency = withdraw.Currency
                    }));
        }

        public async Task Consume(ConsumeContext<AlterAddWithdrawResponse> context)
        {
            if (callbackMapper.IsSuckTaskExist(context.Message.Id))
                callbackMapper.SetValue(context.Message.Id, new AddWithdrawResponse
                {
                    RequestId = context.Message.WithdrawId
                });
        }
    }
}
