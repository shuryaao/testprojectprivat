﻿using PrivatBank.Withdraw.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Services
{
    public interface ICallbackMapperService
    {
        bool AddCallback(string correlationId, TaskCompletionSource<object> task);
        bool IsSuckTaskExist(string correlationId);
        bool SetValue(string correlationId, object obj);
    }
}
