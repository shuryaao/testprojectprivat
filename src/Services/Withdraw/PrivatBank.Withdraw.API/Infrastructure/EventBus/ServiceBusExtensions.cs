﻿using GreenPipes;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PrivatBank.Withdraw.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Infrastructure.EventBus
{
    public static class ServiceBusExtensions
    {
        internal static IServiceCollection AddServiceBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMassTransit(x =>
                {
                    x.AddConsumer<WithdrawService>();
                    x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                        {
                            cfg.Host(configuration["RabbitMQ:Host"], config =>
                                {
                                    config.Username(configuration["RabbitMQ:User"]);
                                    config.Password(configuration["RabbitMQ:Password"]);
                                }
                            );

                            cfg.ReceiveEndpoint(configuration["RabbitMQ:AlterWithdrawAnswerQueue"], ep =>
                            {
                                ep.PrefetchCount = 1;
                                ep.Durable = true;
                                ep.UseMessageRetry(r => r.Interval(2, 100));
                                ep.ConfigureConsumer<WithdrawService>(provider);
                            });

                            cfg.ReceiveEndpoint(configuration["RabbitMQ:AlterGetWithdrawAnswerQueue"], ep =>
                            {
                                ep.PrefetchCount = 1;
                                ep.Durable = true;
                                ep.UseMessageRetry(r => r.Interval(2, 100));
                                ep.ConfigureConsumer<WithdrawService>(provider);
                            });

                            cfg.Durable = true;
                        }
                    ));
                });
            return services.AddMassTransitHostedService();
        }
    }
}
