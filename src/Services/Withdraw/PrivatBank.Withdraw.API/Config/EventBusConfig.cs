﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Config
{
    public class EventBusConfig
    {
        public string AlterWithdrawRequestQueue { get; set; }
        public string AlterGetByIdWithdrawRequestQueue { get; set; }
        public string AlterGetClientWithdrawRequestQueue { get; set; }
    }
}
