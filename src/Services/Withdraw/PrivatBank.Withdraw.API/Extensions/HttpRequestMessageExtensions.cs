﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Extensions
{
    public static class HttpRequestMessageExtensions
    {
        public static string GetClientIpAddress(this HttpRequest request)
        {
            var userip = request.HttpContext.Connection.RemoteIpAddress;
            return userip.MapToIPv4().ToString();
        }
    }
}
