﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PrivatBank.Withdraw.API.Extensions;
using PrivatBank.Withdraw.API.Models;
using PrivatBank.Withdraw.API.Services;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WithdrawController : ControllerBase
    {
        private readonly IWithdrawService withdrawService;
        public WithdrawController(IWithdrawService withdrawService)
        {
            this.withdrawService = withdrawService;
        }
        [HttpPost]
        public async Task<IActionResult> AddWithdraw([FromBody] AddWithdrawRequest withdraw)
        {
            Log.Information(@"New Withdraw Request from {Ip}, request {@WithdrawRequest}", Request.GetClientIpAddress(), withdraw);
            var result = await withdrawService.AddWithdraw(withdraw);
            return Ok(result);
        }

        [HttpGet("byRequestId")]
        public async Task<IActionResult> GetByWithdrawId([FromBody] GetWithdrawByIdRequest withdrawRequest)
        {
            Log.Information(@"New GetWithdrawById Request  from {Ip}, request {@GetWithdrawByIdRequest}", Request.GetClientIpAddress(), withdrawRequest);
            var result = await withdrawService.AddWithdraw(withdrawRequest);
            return Ok(result);
        }

        [HttpGet("byClient")]
        public async Task<IActionResult> GetByClient([FromBody] GetWithdrawByClientRequest withdrawRequest)
        {
            Log.Information(@"New WithdrawByClient Request from {Ip}, request {@GetWithdrawByClientRequest}", Request.GetClientIpAddress(), withdrawRequest);
            var result = await withdrawService.AddWithdraw(withdrawRequest);
            return Ok(result);
        }
    }
}
