using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PrivatBank.Withdraw.Processor.Infrastructure;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.Processor
{
    public class Program
    {
            public static readonly string Namespace = typeof(Program).Namespace;
            public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

            public static async Task<int> Main(string[] args)
            {
                var configuration = GetConfiguration();
                using var seed = new SeedDataBase(configuration);
                await seed.SeedDataAsync();
                try
                {
                    Log.Information("Configuring web host ({ApplicationContext})...", AppName);
                    var host = BuildHost(configuration, args);

                    Log.Information("Starting web host ({ApplicationContext})...", AppName);
                    host.Run();

                    return 0;
                }
                catch (Exception ex)
                {
                    Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
                    return 1;
                }
                finally
                {
                    Log.CloseAndFlush();
                }
            }

            public static IHost BuildHost(IConfiguration configuration, string[] args) =>
                Host.CreateDefaultBuilder(args)
                    .UseSerilog((host, log) =>
                        {
                            if (host.HostingEnvironment.IsProduction())
                                log.MinimumLevel.Information();
                            else
                                log.MinimumLevel.Debug();

                            log.MinimumLevel.Override("Microsoft", LogEventLevel.Warning);
                            log.MinimumLevel.Override("Quartz", LogEventLevel.Information);
                            log.WriteTo.Seq("http://seq-log:5341", apiKey: "none");
                            log.WriteTo.Console();
                        }
                    )
                    .ConfigureWebHostDefaults(webHost =>
                    {
                        webHost.CaptureStartupErrors(false)
                        .ConfigureKestrel(options =>
                        {
                            var (httpPort, healthCheckPort) = GetDefinedPorts(configuration);

                            options.Listen(IPAddress.Any, httpPort, listenOptions =>
                            {
                                listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
                            });

                            if (httpPort != healthCheckPort)
                            {
                                options.Listen(IPAddress.Any, healthCheckPort, listenOptions =>
                                {
                                    listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
                                });
                            }
                        })
                        .UseStartup<Startup>()
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .UseConfiguration(configuration);
                    })
                    .Build();
            private static IConfiguration GetConfiguration()
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddEnvironmentVariables();

                return builder.Build();
            }


            private static (int httpPort, int healthCheckPort) GetDefinedPorts(IConfiguration config)
            {
                var httpPort = config.GetValue("PORT", 5002);
                var healthCheckPort = config.GetValue("HEALTH_CHECK_PORT", 5003);
                return (httpPort, healthCheckPort);
            }

        }
}
