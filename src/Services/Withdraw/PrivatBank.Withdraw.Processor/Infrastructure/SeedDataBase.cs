﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.Processor.Infrastructure
{
    public class SeedDataBase : IDisposable
    {
        private readonly IConfiguration _config;
        private string Connectionstring = "DefaultConnection";

        public SeedDataBase(IConfiguration config)
        {
            _config = config;
        }
        public async Task SeedDataAsync()
        {
            var fileContent = await File.ReadAllTextAsync(Path.Combine("Files", "SqlScripts.txt"));
            var scriptsSql = fileContent.Split("\\\\\\");
            foreach (var script in scriptsSql)
            {
                if (string.IsNullOrEmpty(script))
                    continue;
                await ExecuteScriptAsync(script);
            }
        }

        private async Task ExecuteScriptAsync(string script)
        {
            using var db = new SqlConnection(_config.GetConnectionString(Connectionstring));
                await db.ExecuteAsync(script);
        }

        public void Dispose()
        {

        }
    }
}
