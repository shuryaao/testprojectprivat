﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.Processor.Config
{
    public class EventBusConfig
    {
        public string AlterWithdrawRequestQueue { get; set; }
        public string AlterWithdrawAnswerQueue { get; set; }
        public string AlterGetWithdrawAnswerQueue { get; set; }
    }
}
