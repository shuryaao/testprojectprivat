using GreenPipes;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PrivatBank.Withdraw.Processor.Config;
using PrivatBank.Withdraw.Processor.Consumers;
using PrivatBank.Withdraw.Processor.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.Processor
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<EventBusConfig>(Configuration.GetSection("RabbitMQ"));

            //services.AddDbContext<DataContext.AppContext>(options =>
            //              options.UseSqlServer(
            //                  Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IDapper, Infrastructure.Dapper>();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<AlterWithdrawConsumer>();
                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {

                    cfg.Host(Configuration["RabbitMQ:Host"], rabbitConfig =>
                    {
                        rabbitConfig.Username(Configuration["RabbitMQ:User"]);
                        rabbitConfig.Password(Configuration["RabbitMQ:Password"]);
                    });

                    cfg.ReceiveEndpoint(Configuration["RabbitMQ:AlterWithdrawRequestQueue"], ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.Durable = true;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<AlterWithdrawConsumer>(provider);
                    });

                    cfg.ReceiveEndpoint(Configuration["RabbitMQ:AlterGetByIdWithdrawRequestQueue"], ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.Durable = true;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<AlterWithdrawConsumer>(provider);
                    });

                    cfg.ReceiveEndpoint(Configuration["RabbitMQ:AlterGetClientWithdrawRequestQueue"], ep =>
                    {
                        ep.PrefetchCount = 1;
                        ep.Durable = true;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<AlterWithdrawConsumer>(provider);
                    });

                }));
            });

            services.AddMassTransitHostedService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
