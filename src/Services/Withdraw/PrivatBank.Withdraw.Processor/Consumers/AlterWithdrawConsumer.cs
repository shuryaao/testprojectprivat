﻿using Dapper;
using MassTransit;
using Microsoft.Extensions.Options;
using PrivatBank.EventBus.Commands.Alter;
using PrivatBank.Withdraw.Processor.Config;
using PrivatBank.Withdraw.Processor.Infrastructure;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PrivatBank.Withdraw.Processor.Consumers
{
    //тут можно было бы вытянуть код с работой с базой в отдельную зависимость
    //но уже не стал тратить на это время
    public class AlterWithdrawConsumer  : IConsumer<AlterAddWithdrawRequest>, 
        IConsumer<AlterGetByIdWithdrawRequest>, 
        IConsumer<AlterGetClientWithdrawsRequest>
    {
        private readonly IBus eventBus;
        private readonly EventBusConfig eventBusConfig;
        private readonly IDapper withdrawRepository;

        public AlterWithdrawConsumer(IBus eventBus, IOptions<EventBusConfig> eventBusConfig, IDapper withdrawRepository)
        {
            this.eventBus = eventBus;
            this.eventBusConfig = eventBusConfig.Value;
            this.withdrawRepository = withdrawRepository;
        }

        public virtual async Task Consume(ConsumeContext<AlterAddWithdrawRequest> context)
        {
            var publishEndpoint = await eventBus.GetSendEndpoint(new Uri($"queue:{eventBusConfig.AlterWithdrawAnswerQueue}"));
            var withdrawId = Guid.NewGuid().ToString();
            try
            {
                //Check client in BD
                var dbParams = new DynamicParameters();
                dbParams.Add("ClientId", context.Message.ClientId, DbType.String);
                var clientExist = await Task.FromResult(withdrawRepository.Get<string>("[dbo].[Client_Get]", dbParams));
                if (string.IsNullOrEmpty(clientExist))
                {
                    //Add client to bd
                    dbParams = new DynamicParameters();
                    dbParams.Add("ClientId", context.Message.ClientId, DbType.String);
                    dbParams.Add("DepartemntAddress", context.Message.DepartemntAddress, DbType.String);
                    var insrt = await Task.FromResult(withdrawRepository.Insert<int>("[dbo].[Client_Insert]", dbParams,
                    commandType: CommandType.StoredProcedure));
                }

                //Add withdraw 
                dbParams = new DynamicParameters();
                dbParams.Add("WithdrawId", withdrawId, DbType.String);
                dbParams.Add("ClientId", context.Message.ClientId, DbType.String);
                dbParams.Add("Currency", context.Message.Currency, DbType.String);
                dbParams.Add("Status", "New Request", DbType.String);
                dbParams.Add("Amount", context.Message.Amount, DbType.Decimal);
                var result = await Task.FromResult(withdrawRepository.Insert<int>("[dbo].[Withdraw_Insert]", dbParams,
                    commandType: CommandType.StoredProcedure));

                await publishEndpoint.Send(new AlterAddWithdrawResponse
                {
                    Id = context.Message.Id,
                    WithdrawId = withdrawId
                });
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                await publishEndpoint.Send(new AlterAddWithdrawResponse
                {
                    Id = context.Message.Id,
                    WithdrawId = ""
                });
            }
            
             
        }

        public virtual async Task Consume(ConsumeContext<AlterGetByIdWithdrawRequest> context)
        {
            var publishEndpoint = await eventBus.GetSendEndpoint(new Uri($"queue:{eventBusConfig.AlterGetWithdrawAnswerQueue}"));
            var withdrawId = Guid.NewGuid().ToString();
            try
            {
                var dbParams = new DynamicParameters();
                dbParams.Add("WithdrawId", context.Message.WithdrawId, DbType.String);
                var withdraws = await Task.FromResult(withdrawRepository.GetAll<EventBus.Commands.Alter.Withdraw>("[dbo].[Withdraw_GetById]", dbParams));

                await publishEndpoint.Send(new AlterGetWithdrawsResponse
                {
                    Id = context.Message.Id,
                    Withdraws = withdraws
                });
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                await publishEndpoint.Send(new AlterGetWithdrawsResponse
                {
                    Id = context.Message.Id,
                    Withdraws = Enumerable.Empty<EventBus.Commands.Alter.Withdraw>()
                });
            }
            
        }

        public virtual async Task Consume(ConsumeContext<AlterGetClientWithdrawsRequest> context)
        {
            var publishEndpoint = await eventBus.GetSendEndpoint(new Uri($"queue:{eventBusConfig.AlterGetWithdrawAnswerQueue}"));
            var withdrawId = Guid.NewGuid().ToString();
            try
            {
                var dbParams = new DynamicParameters();
                dbParams.Add("ClientId", context.Message.ClientId, DbType.String);
                var withdraws = await Task.FromResult(withdrawRepository.GetAll<EventBus.Commands.Alter.Withdraw>("[dbo].[Withdraw_GetByClient]", dbParams));

                await publishEndpoint.Send(new AlterGetWithdrawsResponse
                {
                    Id = context.Message.Id,
                    Withdraws = withdraws
                });
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                await publishEndpoint.Send(new AlterGetWithdrawsResponse
                {
                    Id = context.Message.Id,
                    Withdraws = Enumerable.Empty<EventBus.Commands.Alter.Withdraw>()
                });
            }
        }
    }
}
